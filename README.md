# Developer Coding Assignment
### By Md. Yeasin Hossain

# Prerequisits
  - node.js
  - npm
  - mongoDB
  

# Project setup
Issue following commands to instal the dependencies:
```sh
$ npm install
```

# Running the application
Issue the following command to bootup the server, make sure that mongo DB server is running on default (27017) port:
```sh
$ sails lift
```

This will start the server at http://localhost:1337

# Available endpoints
- POST http://localhost:1337/Track/1/anon body: {any thing}
- POST http://localhost:1337/Track/1/person body: email
- POST http://localhost:1337/Track/1/lead body: email
- GET http://localhost:1337/Visitors/1?city={city name}&gender={male|female}&isLead={y|n}

# Running the test
Run following command to run test:
```sh
$ npm test
```