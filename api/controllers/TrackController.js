/**
 * TrackController
 *
 * @description :: Server-side logic for managing Tracks
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  
  //    Handles request for Track/:page/anon
  anon: function (req, res) {
    var userInfo = {
      headers: req.headers, ip: req.ip, ips: req.ips, cookies: req.cookies, browserData: req.body };
    sails.models.anon.create(userInfo).exec(function (err, anon) {        
        if (err) return res.serverError(err);
        return res.json(anon);
    });    
  },
  
  //    Handles request for Track/:page/person
  person: function (req, res) {
      var email = req.body.email;      
      if( !UtilService.isEmail(email) ) return res.json({msg: "Invalid email"});
      
      ClearbitService.getPersonByEmail(email).then(function (person) {          
          person.isLocal = UtilService.isLocal(person.geo.city);
          sails.models.person.findOrCreate({email: email}, person).exec(function(err, person){
              if (err) { return res.serverError(err); }
              return res.json(person);
          });
      }).catch(function (err) {
          return res.serverError(err);
      });
  },
  
  //    Handles request for Track/:page/lead
  lead: function (req, res) {
      var email = req.body.email;

      if( !UtilService.isEmail(email) ) return res.json({msg: "Invalid email"});

      sails.models.person.findOne({email: email}).exec(function(err, person){
          if (err) { return res.serverError(err); }

          let isLead = UtilService.isLead(person);
          sails.models.person.update({email: email}, {isLead: isLead}).exec(function(err, updatedPerson){
              if (err) { return res.serverError(err); }              
              return res.json(updatedPerson);
          });
      })

  }
};

