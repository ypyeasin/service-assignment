/**
 * VisitorsController
 *
 * @description :: Server-side logic for managing Visitors
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  
  //    Handles request for Visitors/:page
  index: function (req, res) {
    var condition = UtilService.prepeareSearchCriteria(req.query);    
    sails.models.person.find(condition).exec(function(err, persons){
        if (err) return res.serverError(err);
        return res.json(persons);
    });    
  }
  
};

