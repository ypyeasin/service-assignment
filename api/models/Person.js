/**
 * Person.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'person',
  connection: 'mongodbServer',
  attributes: {
    email: {
      type: 'string',
      unique: true,
      required: true,
      email: true
    },
    isLocal: {
      type: 'boolean',
      defaultsTo: false
    },
    isLead: {
      type: 'boolean',
      defaultsTo: false
    }
  }
};

