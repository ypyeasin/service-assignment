var clearbit = require('clearbit')( sails.config.clearbit.apiKey);
var Person = clearbit.Person;

module.exports = {

    getPersonByEmail: function(email){
        return Person.find({email: email});
    }

}