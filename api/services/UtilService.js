var validator = require('validator');
var _ = require('lodash');

module.exports = {

    isEmail: function(email){
        return validator.isEmail(email);
    },

    isLocal: function(city){
        return city == sails.config.clearbit.localCity;
    },

    isLead: function(data){
        data = data || {};
        if( _.isEmpty(data) ) return true;
        if( data.gender.toLowerCase() == 'female' && data.utcOffset == -7) return false;
        return true;
    },

    prepeareSearchCriteria: function(query){
        var criteria = _.pick(query, ['city', 'gender', 'isLead']);

        if( !!criteria.city ){
            criteria['geo.city'] = criteria.city;
            delete criteria.city;
        }

        if( !!criteria.isLead ){
            criteria.isLead = criteria.isLead.toLowerCase() == 'y' ? true : false;
        }

        //  Removing empty values
        for(var i in criteria){
            if( _.isEmpty(criteria[i]) )
                delete criteria[i];
        }

        return criteria;
    }

}