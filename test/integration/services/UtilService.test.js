var assert = require('assert');

describe('UtilService', function(){

    //  Test for isLocal method
    describe('checking isLocal method', function() {
        it('should return false for empty string', function (done) {
            var isLocal = UtilService.isLocal('');
            assert.equal(isLocal, false);
            done();
        });

        it('should return false for null', function (done) {
            var isLocal = UtilService.isLocal(null);
            assert.equal(isLocal, false);
            done();
        });

        it('should return false for undefined', function (done) {
            var isLocal = UtilService.isLocal();
            assert.equal(isLocal, false);
            done();
        });

        it('should return false for dhaka', function (done) {
            var isLocal = UtilService.isLocal('Dhaka');            
            assert.equal(isLocal, false);
            done();
        });

        it('should return true for San Francisco', function (done) {
            var isLocal = UtilService.isLocal('San Francisco');            
            assert.equal(isLocal, true);
            done();
        });
    });

    //  Test for isLead method
    describe('Checking isLead method', function(){

        it("should return false for female in MST (UTC-7)", function(){
            var data = {gender: 'female', utcOffset: -7};
            var isLead = UtilService.isLead(data);
            assert.equal(isLead, false);
        })

        it("should return true for undefined", function(){
            var data = undefined;
            var isLead = UtilService.isLead(data);
            assert.equal(isLead, true);
        })

        it("should return true for null", function(){
            var data = undefined;
            var isLead = UtilService.isLead(data);
            assert.equal(isLead, true);
        })

    })

})


